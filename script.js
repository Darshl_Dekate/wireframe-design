// Function to make the popup draggable
function dragElement(elmnt) {
    let pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    if (document.getElementById(elmnt.id + "-header")) {
        // If present, the header is where you move the DIV from:
        document.getElementById(elmnt.id + "-header").onmousedown = dragMouseDown;
    } else {
        // Otherwise, move the DIV from anywhere inside the DIV:
        elmnt.onmousedown = dragMouseDown;
    }

    function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        // Get the mouse cursor position at startup:
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        // Call a function whenever the cursor moves:
        document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        // Calculate the new cursor position:
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        // Set the element's new position:
        elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
    }

    function closeDragElement() {
        // Stop moving when mouse button is released:
        document.onmouseup = null;
        document.onmousemove = null;
    }
}

// Make the popup draggable
dragElement(document.getElementById("popup"));

// Function to open the popup
document.getElementById("save-button").addEventListener("click", function () {
    document.getElementById("popup").style.display = "block";
});

// Function to make sections resizable
function makeResizable(elmnt, bar) {
    let startX, startWidth;

    bar.addEventListener("mousedown", function(e) {
        e.preventDefault();
        startX = e.clientX;
        startWidth = parseFloat(getComputedStyle(elmnt, null).width);
        
        document.addEventListener("mousemove", resize);
        document.addEventListener("mouseup", stopResize);
    });

    function resize(e) {
        const width = startWidth + (e.clientX - startX);
        elmnt.style.width = width + "px";
    }

    function stopResize() {
        document.removeEventListener("mousemove", resize);
        document.removeEventListener("mouseup", stopResize);
    }
}

// Make sections resizable
const section1 = document.getElementById("section1");
const section2 = document.getElementById("section2");
const section3 = document.getElementById("section3");
const resizableBar1 = document.getElementById("resizable-bar1");
const resizableBar2 = document.getElementById("resizable-bar2");

makeResizable(section1, resizableBar1);
makeResizable(section2, resizableBar2);
